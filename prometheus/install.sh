# 创建相关目录并给予权限，持久化目录需要给777权限，否则容器启动失败
CURDIR=$(pwd)
rm -rf /opt/prometheus
mkdir /opt/prometheus && chmod 777 /opt/prometheus
cd /opt/prometheus && mkdir grafana_data prometheus_data && chmod 777 grafana_data prometheus_data
cp $CURDIR/prometheus.yml /opt/prometheus/prometheus.yml

docker stop prometheus0 node-exporter0 grafana0

cd $CURDIR
docker compose up -d     # 挂载目录给权限，不然会启动失败