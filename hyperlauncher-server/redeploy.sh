# self update
git pull

# delete old config.ron
rm -rf /opt/hyperlauncher/config

# stop and delete old container and image
docker stop hyperealm-launcher-server-latest
docker rm hyperealm-launcher-server-latest
docker rmi -f registry.gitlab.com/hyperworld1/hyperealm-launcher-server/hyperealm-launcher-server:main

# login registry
docker login -u hyperworld-official -p zl13177211191 registry.gitlab.com

# start docker image
docker compose up -d

# restart hyperealm-launcher-server to generate new template config

# make new config.ron
sudo cp /opt/hyperlauncher/config/config.template.ron /opt/hyperlauncher/config/config.ron
# restart to work
docker restart hyperealm-launcher-server-latest
