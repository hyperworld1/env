# install docker and compose
# curl -fsSL https://get.docker.com -o get-docker.sh
# sh get-docker.sh

# sudo apt install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin

# 1. pull the env scripts
git clone https://gitlab.com/hyperworld1/env.git
cd env

# 2. change source
./Ubuntu-China-Source/ChangeSource.sh

# 3. install docker and compose
./docker-install/install.sh


# 4. copy database file
cd env/hyperauth
mkdir -p /opt/hyperauth/data
cp db/auth.db /opt/hyperauth/data/auth.db
chmod 777 /opt/hyperauth/data/auth.db

# 5. dploy hyperauth-server
chmod +x redeploy.dh
./redeploy.sh

# setup npm
cd npm
docker compose up -d

