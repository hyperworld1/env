#!/bin/sh

echo "*********************************"
sudo echo "choose ubuntu version:"
echo "  14.04 input 1"
echo "  16.04 input 2"
echo "  18.04 input 3"
echo "  20.04 input 4"
echo "*********************************"
read SystemIndex

if [ -z $SystemIndex ];then
    echo "Wrong format"
    exit
 else
    index=`echo "$SystemIndex*1" | bc `
   if [ $index -eq 1 ];then
       System=14.04
   elif [ $index -eq 2 ];then
       System=16.04   
   elif [ $index -eq 3 ];then  
       System=18.04 
   elif [ $index -eq 4 ];then  
       System=20.04 
   fi
fi

echo "*********************************"
echo "Choose source:"
echo "  Ali input 1"
echo "  qinghua input 2"
echo "  163 input 3"
echo "  ustc input 4"
echo "*********************************"
read SourceIndex

if [ -z $SystemIndex ];then
    echo "Wrong format"
    exit
 else
    index=`echo "$SourceIndex*1" | bc `
   if [ $index -eq 1 ];then
	echo 1111
       Source=ali
   elif [ $index -eq 2 ];then
	echo 222
       Source=qinghua   
   elif [ $index -eq 3 ];then  
       Source=163 
   elif [ $index -eq 4 ];then  
       Source=ustc 
    fi
fi

echo $Source
filename="source_${Source}_ubuntu${System}.list"

echo "backup..."
sudo cp /etc/apt/sources.list /etc/apt/sources_init.list
echo "copy..."
sudo cp $filename /etc/apt/sources.list
echo "update..."
sudo apt-get update
# echo "install..."
# sudo apt-get -f install
# echo "upgrade..."
# sudo apt-get upgrade
echo "Done"

